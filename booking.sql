CREATE TABLE booking (
    booking_id INTEGER PRIMARY KEY AUTOINCREMENT,
    booking_date DATE NOT NULL,
    booking_time TIME NOT NULL,
);


CREATE TABLE vaccine (
	vaccine_id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL UNIQUE ,
	instructions TEXT NOT NULL,
	pharm_company TEXT NOT NULL,
	booking INTEGER NOT NULL,
	FOREIGN KEY(booking) REFERENCES booking(booking_id) DELETE ON CASCADE
	);


CREATE TABLE vaccine_point (
   vaccine_point_id INTEGER PRIMARY KEY AUTOINCREMENT,
	city TEXT NOT NULL,
	name TEXT NOT NULL,
	booking INTEGER NOT NULL,
	FOREIGN KEY(booking) REFERENCES booking(booking_id) DELETE ON CASCADE
);

CREATE TABLE vaccine_user (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	booking INTEGER NOT NULL,
	FOREIGN KEY(booking) REFERENCES booking(booking_id) DELETE ON CASCADE
);


SELECT *
FROM vaccine
where vaccine_id=1;