FROM python:gi-buster
RUN apt-get update && apt-get -y install sqlite3
ENV HOST '0.0.0.0'
ENV PORT 5000
ENV SQLITE3_PATH booking.db
COPY app.py /usr/app/
COPY requirements.txt /usr/app/
COPY booking.sql /usr/app
WORKDIR /usr/app
RUN pip install -r requirements.txt
RUN sqlite3 booking.db < booking.sql
CMD python app.py