from flask import Flask, request, jsonify
import sqlite3
import os
import sys
import mysql

# Init app: ricordati di inserire nome della cartella Github
#Flask framework per web application
app = Flask("bonanippaffenvilla")



#generate dictionary from db results
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


#La funzione restituisce se sto lavorando
# con Mysql oppure SqLite3. I metodi di
#gestione dei due database sono uguali.


def get_db_cursor():
    if os.getenv("MYSQL_HOST",None) != None:
        mydb = mysql.connector.connect(
            host=os.getenv("MYSQL_HOST","mydb"),
            user=os.getenv("MYSQL_USER","dev-user"),
            password=os.getenv("MYSQL_PASSWORD","password"),
            database=os.getenv("MYSQL_DATABASE","cocktails")
        )
        return mydb
    elif os.getenv("SQLITE3_PATH","cockail.db") != None:
        return sqlite3.connect(os.getenv("SQLITE3_PATH","cockail.db"))
    else:
        sys.exit("Errors: app.py needs DB parameters (MYSQL or SQLITE).")



@app.route('/', methods=['GET'])
def home():
    return "<h1>Questo è il progetto d'esame. Le api sono disponibili a /api/v1/</h1>"


# --------RESTITUISCE TUTTI I VACCINI E NE CREA UNO NUOVO----------
@app.route('/api/v1/vaccines/', methods=['GET','POST'])
def vaccine():
    #----Si implementa il metodo GET di http che RESTITUISCE LA LISTA DI VACCINI ------
    if request.method == "GET":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        conn.row_factory = dict_factory
        cur = conn.cursor()
        vaccines = cur.execute('SELECT * FROM vaccine;').fetchall()
        return jsonify(vaccines),200
    # ----Si implementa il metodo POST di http PER AGGIUNGERE UN NUOVO VACCINO ALLA LISTA-----
    elif request.method == "POST":
        if not request.is_json:
            return {"error":"only json accepted"},400
        content = request.get_json()
        name = content.get('name')
        instructions = content.get('instructions')
        pharm_company = content.get('pharm_company')

        conn = get_db_cursor()
        cur = conn.cursor()

        try:
            query = 'INSERT INTO vaccine (name, instructions, pharm_company) VALUES ("' + name + '","' + instructions + '","' + pharm_company + '");'
            cur.execute(query)
        except sqlite3.IntegrityError as err:
            return jsonify({"Error":"name must be unique"}),409
        id=cur.lastrowid
        conn.commit()
        return jsonify({"id":id}),201




# ----------RESTITUISCE TUTTI I PUNTI DI VACCINO E NE CREA UNO NUOVO--------
@app.route('/api/v1/vaccine_points/', methods=['GET','POST'])
def vaccine_point():
    #----Si implementa il metodo GET di http che RESTITUISCE LA LISTA DI PUNTI DI VACCINO ------
    if request.method == "GET":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        conn.row_factory = dict_factory
        cur = conn.cursor()
        vaccine_points = cur.execute('SELECT * FROM vaccine_point;').fetchall()
        return jsonify(vaccine_points),200

    # ----Si implementa il metodo POST di http PER AGGIUNGERE UN NUOVO PUNTO DI VACCINO ALLA LISTA-----
    elif request.method == "POST":
        if not request.is_json:
            return {"error":"only json accepted"},400
        content = request.get_json()
        name = content.get('name')
        city = content.get('city')
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        cur = conn.cursor()
        try:
            query = 'INSERT INTO vaccine_point (city,name) VALUES ("' + name + '","' + city + '");'
            cur.execute(query)
        except sqlite3.IntegrityError as err:
            return jsonify({"error":"name must be unique"}),409
        id_point=cur.lastrowid
        conn.commit()
        return jsonify({"id":id_point}),201


#----INDIVIDUATO IL PUNTO DI VACCINO id, SI PUO'-------:
@app.route('/api/v1/vaccine_points/<id>', methods=['GET','DELETE'])

def single_vaccine_point(id):

    #----LEGGERLO----
    if request.method == "GET":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        conn.row_factory = dict_factory
        cur = conn.cursor()
        vaccine_point = cur.execute('SELECT * FROM vaccine_point WHERE vaccine_point_id='+str(id)+';').fetchall()
        if len(vaccine_point) == 0 :
            return "",404
        vaccine_point=vaccine_point[0]
        return jsonify(vaccine_point), 200
    # ----CANCELLARLO----
    elif request.method == "DELETE":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        cur = conn.cursor()
        cur.execute('DELETE FROM vaccine_point WHERE vaccine_point_id=' + str(id) + ';').fetchall()
        conn.commit()
        return "",204



# ----------RESTITUISCE TUTTE LE PRENOTAZIONI E NE CREA UNA NUOVA--------
@app.route('/api/v1/booking/', methods=['GET','POST'])
def booking():
    #----Si implementa il metodo GET di http che RESTITUISCE LA LISTA DI PRENOTAZIONI ------
    if request.method == "GET":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        conn.row_factory = dict_factory
        cur = conn.cursor()
        bookings = cur.execute('SELECT * FROM booking;').fetchall()
        for booking in bookings:
            id=booking["booking_id"]
            vaccine = cur.execute('SELECT name,instructions,pharm_company FROM vaccine WHERE booking=' + str(id) + ';').fetchall()
            vaccine_user = cur.execute('SELECT name FROM vaccine_user WHERE booking=' + str(id) + ';').fetchall()
            vaccine_point = cur.execute('SELECT city,name FROM vaccine_point WHERE booking=' + str(id) + ';').fetchall()
            booking["vaccine"] = vaccine
            booking["vaccine_user"] = vaccine_user
            booking["vaccine_point"] = vaccine_point
        return jsonify(bookings),200

    # ----Si implementa il metodo POST di http PER AGGIUNGERE UN NUOVO PUNTO DI VACCINO ALLA LISTA-----
    elif request.method == "POST":
        if not request.is_json:
            return {"error":"only json accepted"},400
        content = request.get_json()
        booking_date = content.get('booking_date')
        booking_time = content.get('booking_time')
        vaccines= content.get('vaccine')
        vaccine_points = content.get('vaccine_point')
        vaccine_users = content.get('vaccine_user')

        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        cur = conn.cursor()
        try:
            query = 'INSERT INTO booking (booking_date,booking_time) VALUES ("' + booking_date + '","' + booking_time + '");'
            cur.execute(query)
        except sqlite3.IntegrityError as err:
            return jsonify({"error":"name must be unique"}),409
        id = cur.lastrowid
        for vaccine in vaccines:
            try:
                query = 'INSERT INTO vaccine (name, instructions, pharm_company) VALUES ("' + vaccine["name"] + '","' + \
                        vaccine["instructions"] + '","' + vaccine["pharm_company"] + '", ' + str(id) + ');'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        for vaccine_point in vaccine_points:
            try:
                query = 'INSERT INTO vaccine_point (city,name) VALUES ("' + vaccine_point["city"] + '","' + vaccine_point["name"] + '",' + str(id) + ');'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        for user in vaccine_users:
            try:
                query = 'INSERT INTO vaccine_user name VALUES ("' +user + '",' + str(id) + ');'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        conn.commit()
        return jsonify({"id": id}), 201






#----INDIVIDUATA LA PRENOTAZIONE id, SI PUO'-------:
@app.route('/api/v1/booking/<id>', methods=['GET','PUT','DELETE'])

def single_booking(id):

    #----LEGGERLA----
    if request.method == "GET":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists
        conn.row_factory = dict_factory
        cur = conn.cursor()
        booking = cur.execute('SELECT * FROM booking WHERE booking_id=' + str(id) + ';').fetchall()
        if len(booking) == 0:
            return "", 404
        booking = booking[0] #booking id
        vaccine = cur.execute('SELECT (name,instructions,pharm_company) FROM vaccine WHERE booking=' + str(id) + ';').fetchall()
        user = cur.execute('SELECT name FROM user WHERE booking=' + str(id) + ';').fetchall()
        vaccine_point = cur.execute('SELECT (date,time) FROM vaccine_point WHERE booking=' + str(id) + ';').fetchall()
        booking["vaccine"] = vaccine
        booking["user"] = user
        booking["vaccine_point"] = vaccine_point
        return jsonify(booking), 200
    # SE IL METODO è PUT
    elif request.method == "PUT":
    #legge i dati della nuova prenotazione per la modifica successiva solo da file json
        if not request.is_json:
            return {"error":"only json accepted"},400
        content = request.get_json()
        booking_date = content.get('booking_date')
        booking_time = content.get('booking_time')
        vaccines= content.get('vaccine')
        vaccine_points = content.get('vaccine_point')
        vaccine_users = content.get('vaccine_user')

        conn = get_db_cursor()
        cur = conn.cursor()

    #individua la prenotazione da modificare tramite id fornito e la modifica con UPDATE
        try:
            query = 'UPDATE booking SET booking_date = ("' + booking_date + '") AND booking_time = ("' + booking_time + '") WHERE booking_id = ' + str(id) + ' ;'
            cur.execute(query)
        except sqlite3.IntegrityError as err:
            return jsonify({"error":"name must be unique"}),409
        for vaccine in vaccines:
            try:
                query = 'UPDATE vaccine SET ' \
                    'name="' + vaccine["name"] + '" AND instructions= " '+vaccine["instructions"] + '" ' \
                    'AND pharm_company ="' + vaccine["pharm_company"] + '" WHERE booking_id = ' + str(id) + ';'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        for vaccine_point in vaccine_points:
            try:
                query = 'UPDATE vaccine_point SET ' \
                        'name="' + vaccine_point["name"] + '" AND city= " ' + vaccine_point["city"] + '"  WHERE booking_id = ' + str(id) + ';'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        for user in vaccine_users:
            try:
                query = 'UPDATE vaccine_user SET name = "' + user + '" WHERE booking_id= ' + str(id) + ' ;'
                cur.execute(query)
            except sqlite3.IntegrityError as err:
                return jsonify({"error": "name must be unique"}), 409
        conn.commit()
        return "",200
    elif request.method == "DELETE":
        conn = get_db_cursor()
        # returns items from the database as dictionaries rather than lists

        cur = conn.cursor()
        cur.execute('DELETE FROM booking WHERE booking_id=' + str(id) + ';').fetchall()
        conn.commit()
        return "",204


@app.errorhandler(404)
def page_not_found(e):
    return "errore 404 - File Non Trovato", 404

@app.errorhandler(403)
def forbidden(e):
    return "errore 403 - Accesso Negato",403

@app.errorhandler(500)
def internal_server_error(e):
    return "errore 500 - Errore interno al server",500


if __name__ == "__main__":
    port = int(os.getenv("PORT", 5000))
    host = os.getenv("HOST", '0.0.0.0')
    app.run(host=host, port=port)