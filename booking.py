from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask("bonanippaffenvilla")
db = SQLAlchemy(app)


class booking(db.Model):
    __tablename__="booking"
    booking_id = db.Column(db.Integer, primary_key=True, nullable=False)
    booking_date = db.Column(db.Date(80), nullable=False)
    booking_time = db.Column(db.Time(120), nullable=False)
    vaccine = db.relationship('vaccine', backref='booking', lazy=True)
    vaccine_point = db.relationship('vaccine_point', backref='booking', lazy=True)
    vaccine_user = db.relationship('vaccine_user', backref='booking', lazy=True)
    def __repr__(self):
        return '<booking %r>' % self.booking_id

class vaccine(db.Model):
    __tablename__ = "vaccine"
    vaccine_id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.Varchar2(100), nullable=False)
    instructions = db.Column(db.Varchar2(1000), nullable=False)
    pharm_company= db.Column(db.Varchar2(100), nullable=False)
    booking_id = db.Column(db.Integer, db.ForeignKey('booking.booking_id'), nullable=False)
    def __repr__(self):
        return '<vaccine %r>' % self.vaccine_id

class vaccine_point(db.Model):
    __tablename__ = "vaccine_point"
    vaccine_point_id = db.Column(db.Integer, primary_key=True, nullable=False)
    city = db.Column(db.Varchar2(100), nullable=False)
    name = db.Column(db.Varchar2(100), nullable=False)
    booking_id = db.Column(db.Integer, db.ForeignKey('booking.booking_id'), nullable=False)
    def __repr__(self):
        return '<vaccine_point %r>' % self.vaccine_point_id

class vaccine_user(db.Model):
    __tablename__ = "vaccine_user"
    user_id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.Varchar2(100), nullable=False)
    booking_id = db.Column(db.Integer, db.ForeignKey('booking.booking_id'), nullable=False)
    def __repr__(self):
        return '<vaccine_user %r>' % self.vaccine_user

db.create_all()

